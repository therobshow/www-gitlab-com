---
layout: markdown_page
title: "Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### DevOps Discussion with Gary Gruver      
**August 2nd** - 9am PT / 4pm UTC     
[Register today](/webcast/gary-gruver-discussion/) and join Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he shares his experience consulting with organizations to successfully adopt a DevOps model. He’ll offer advice on the influencers who should be involved in discussions and pinpoint the areas were a transformation should begin.

### Release Radar: Security 
**August 8th** - 9am PT / 4pm UTC    
[Register now](/webcast/monthly-release/gitlab-11.1---security/) to join us for a live broadcast on August 8 to learn more about GitLab’s security features including the newly released dashboard.


## Past Webcasts    

### Release Radar: Auto DevOps (June 2018)   
[Watch](/webcast/monthly-release/gitlab-11.0---auto-devops/) the recorded broadcast to learn how Auto DevOps simplifies your deployment pipeline to accelerate delivery by 200%, improves responsiveness, and closes the feedback gap between you and your users.    

### Starting and Scaling DevOps with Gary Gruver     
[Watch](/webcast/starting-scaling-devops/) the webcast with Gary Gruver, author of Starting and Scaling DevOps in the Enterprise, as he covers how to analyze your current deployment pipeline to target your first improvements on the largest inefficiencies in software development and deployment.

### Overcoming Barriers to DevOps Automation - a 4-part series
**Session 1**: Let's Talk DevOps and Drawbacks   
**Session 2**: Solving the Collaboration Conundrum: How to Make the DevOps Dream a Reality    
**Session 3**: Removing Barriers Between Dev and Ops: Setting Up a CI/CD Pipeline   
**Session 4**: Getting Started with CI/CD on GitLab   

[Watch](/webcast/devops-automation/) the webcast recordings. 
